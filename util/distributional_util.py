from gensim import models
import numpy as np


def load_w2v_model_stock_twits(model_path):
    return models.Word2Vec.load(model_path,)

def load_w2v_model_financial_phrases(model_path):
    return models.Word2Vec.load_word2vec_format(model_path,binary=True)


def get_distributional_vector_sentence(tokenized_sentence,w2v_model, vector_size):
    fixed_sentence_length = 100
    in_tuple = (1,fixed_sentence_length,vector_size)
    w2v_tokenized_sentence = np.empty(in_tuple, dtype= "float")
    w2v_tokenized_sentence.fill(-1.0)

    for i,token in enumerate(tokenized_sentence):
        if token in w2v_model:
           vec = w2v_model[token]
        else:
           vec = [-1.0] * vector_size

        w2v_tokenized_sentence[0][i] = vec
    return w2v_tokenized_sentence

