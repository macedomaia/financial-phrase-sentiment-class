import h5py
from keras.models import model_from_json



def load_model(model_arch_path,model_weight_path):
    with open(model_arch_path, 'r') as content_file:
         content = content_file.read()
    model = model_from_json(content)
    model.load_weights(model_weight_path)
    return model
