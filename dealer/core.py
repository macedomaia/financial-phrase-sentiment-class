from keras.models import model_from_json
import os
import sys
PATH = '/home/macedomaia/'
sys.path.append(os.path.abspath(PATH +'financial-phrase-sentiment-class'))
from util.distributional_util import get_distributional_vector_sentence,load_w2v_model_financial_phrases
from util.string_util import load_model

class Dealer(object):

    def __init__(self,model_arch_path, model_weight_path, w2v_model):
        self.model = load_model(model_arch_path, model_weight_path)
        self.w2v_model = w2v_model
        self.w2v_model_loaded = load_w2v_model_financial_phrases(self.w2v_model)


    def predict_sentence(self, sentence):
        tokenized_sentence = sentence.split()
        return self.model.predict_classes(get_distributional_vector_sentence(tokenized_sentence,self.w2v_model_loaded, self.w2v_model_loaded.vector_size))
