import os
import sys
PATH = '/home/macedomaia/'
sys.path.append(os.path.abspath(PATH +'financial-phrase-sentiment-class'))
from dealer.core import Dealer
from util.string_util import load_model
from util.distributional_util import load_w2v_model_stock_twits

model_arch_path = "../model_finances/stock_arch.json"
model_weight_path = "../model_finances/stock_weights.h5"

class Stocktwits(Dealer):
    def __init__(self, w2v_model_path):

        Dealer.model = load_model(model_arch_path,model_weight_path)
        Dealer.w2v_model_loaded = load_w2v_model_stock_twits(w2v_model_path)

    def convert_to_nominal(self,number):
        number_to_emotion = {
            0: 'Bullish',
            1: 'Bearish'
        }
        return number_to_emotion[int(number)]

