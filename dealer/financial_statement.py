import os
import sys
PATH = '/home/macedomaia/'
sys.path.append(os.path.abspath(PATH +'financial-phrase-sentiment-class'))
from dealer.core import Dealer
from util.string_util import load_model
from util.distributional_util import load_w2v_model_financial_phrases

model_arch_path = "../model_finances/financial_arch.json"
model_weight_path = "../model_finances/financial_weights.h5"

class FinancialStatementDealer(Dealer):
    def __init__(self, w2v_model_path):
        Dealer.model = load_model(model_arch_path,model_weight_path)
        Dealer.w2v_model_loaded =  load_w2v_model_financial_phrases(w2v_model_path)



    def convert_to_nominal(self,number):
        number_to_emotion = {
            0: 'neutral',
            1: 'negative',
            2: 'positive'
        }
        return number_to_emotion[int(number)]



