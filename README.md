# Finance Phrases Sentiment Classifier
--------------------------

### The goal is to classify sentences using sentiment labels


#### How to install (Ubuntu)


- Installing Python3, Git, HDF5 and pip3
  
  - sudo apt-get install python3

  - sudo apt-get install git

  - sudo apt-get install python3-setuptools
  
  - sudo apt-get install python3-pip
  
  - sudo apt-get install libhdf5-dev

- Installing some basic python libs

  - sudo pip3 install --upgrade pip
  
  - sudo pip3 install numpy

  - sudo pip3 install pandas

  - sudo pip3 install gensim

  - sudo pip3 install h5py


- Installing theano and keras

  - sudo pip3 install theano

  - sudo pip3 install keras==0.3.2

- Now you should download the project

  - git clone https://gitlab.com/macedomaia/financial-phrase-sentiment-class.git

#### How to use
- Before running, you have to download the w2v model files. You should click [here](https://drive.google.com/open?id=0B8ocM1F5V0-zMDJFQmhwVm1xZzA) and download every 3 files.
- save the files in a folder (e.g. /home/macedomaia/stock_w2v_model_folder/)
- acessing the project directory  
 - cd /path-to-project/financial-phrase-sentiment-class/demo
- Running the sentiment classifier passing the stock w2v model path as a parameter:
 - python3 Demo.py /path-to-model/stock_w2v_model (e.g.  python3 Demo.py /home/macedomaia/stock_w2v_model_folder/stock_w2v_model)
  