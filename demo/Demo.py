import os
import sys
current_folder_path, current_folder_name = os.path.split(os.getcwd())
sys.path.append(os.path.abspath(current_folder_path))
from dealer.financial_statement import FinancialStatementDealer
from dealer.stocktwits import Stocktwits
from textblob import TextBlob


w2v_model_stock = "/home/macedomaia/stock_w2v_model"
#w2v_model_google_news = "/home/macedomaia/Downloads/GoogleNews-vectors-negative300.bin"

stocktwitsdealer = Stocktwits(w2v_model_stock)

#financialdealer  = FinancialStatementDealer(w2v_model_google_news)



while True:
    try:
       mode= input('Typing some sentence:')
    except ValueError:
       print("it's not a string")

    blob = TextBlob(mode)

    language = blob.detect_language()

    if language != "en":
        mode = blob.translate(to="en")
        print(mode)

    predictionstock = stocktwitsdealer.predict_sentence(mode)
    #predictionfinance = financialdealer.predict_sentence(mode)

    #labelfin = financialdealer.convert_to_nominal(predictionfinance)
    labelstock = stocktwitsdealer.convert_to_nominal(predictionstock)

    #print(labelfin)
    print(labelstock)
